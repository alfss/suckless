//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	/* {"⌨", "sb-kbselect", 0, 30}, */
	{"", "cat /tmp/recordingicon 2>/dev/null",	0,	9},
	/* {"",	"sb-tasks",	10,	26}, */
	/* {"",	"sb-news",		0,	6}, */
	/* {"",	"sb-price lbc \"LBRY Token\" 📚",			9000,	22}, */
	/* {"",	"sb-price bat \"Basic Attention Token\" 🦁",	9000,	20}, */
	/* {"",	"sb-price link \"Chainlink\" 🔗",			300,	25}, */
	/* {"",	"sb-price xmr \"Monero\" 🔒",			9000,	24}, */
	/* {"",	"sb-price eth Ethereum 🍸",	9000,	23}, */
	/* {"",	"sb-price btc Bitcoin 💰",				9000,	21}, */
	/* {"",	"sb-moonphase",	18000,	17}, */
	/* {"",	"sb-mailbox",	180,	12}, */
	{"│CPU:",	"sb-cpu",		10,	18}, 
	{"",	"sb-cpubars",		2,	18}, 
	{"│",	"sb-memory",		4,	14}, 
	{"│",	"sb-disk",			60,	14}, 
	{"",	"sb-pacpackages",	0,	8},
	{"│",	"sb-nettraf",		1,	16},
	{"",	"sb-torrent",		10,	7},
	{"",	"sb-internet",		10,	4},
	{"|",	"sb-brightness",	0,	11},
	{"",	"sb-volume",		0,	10},
	{" ",	"sb-music",			0,	13}, 
	{"",	"sb-bluetooth",		10,	6},
	{"",	"sb-battery",		6,	3},
	{"│ ",	"sb-forecast",		18000,	5},
	{"",	"sb-clock",			2,	1},
	{"│",	"sb-help-icon",		0,	15},
};

//Sets delimiter between status commands. NULL character ('\0') means no delimiter.
static char *delim = " ";

// Have dwmblocks automatically recompile and run when you edit this file in
// vim with the following line in your vimrc/init.vim:

// autocmd BufWritePost ~/.local/src/dwmblocks/config.h !cd ~/.local/src/dwmblocks/; sudo make install && { killall -q dwmblocks;setsid dwmblocks & }
